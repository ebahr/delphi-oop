unit uEncapsulation;

interface

type
  TEmployee = class
  private
    //encapsulated properties - we need to protect them as much as possible!!!
    FFirstName,
    FLastName: String;
    FAge: Integer;
    function GetFullName: String;

  public
    //properties available to be used
    property FirstName: String read FFirstName write FFirstName;
    property LastName: String read FLastName write FLastName;
    property FullName: String read GetFullName;
    property Age: Integer read FAge write FAge;
  end;

implementation

uses
  sysutils;

const
  FULL_NAME_TEMPLATE = '%s %s';

{ TEmployee }

function TEmployee.GetFullName: String;
begin
  result := Format(FULL_NAME_TEMPLATE, [FFirstName, FLastName])
end;

end.
