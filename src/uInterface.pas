unit uInterface;

interface

type
  IGeometricForm = interface
  //ctrl + shift + g to generate the GUID
  ['{41B84351-F02F-4FBF-8862-7DB59EF4A9FD}']

    function GetArea: double;
    function GetName: string;
  end;

  TRectangle = class(TInterfacedObject, IGeometricForm)
  private
    FWidht,
    FLength: Double;

  public
    constructor Create(const dWidht, dLength: double);
    function GetArea: double;
    function GetName: string;

    property Name: string read GetName;
  end;



implementation

{ TRectangle }

constructor TRectangle.Create(const dWidht, dLength: double);
begin
  FWidht := dWidht;
  FLength := dLength;
end;

function TRectangle.GetArea: double;
begin
  result := FLength * FWidht;
end;

function TRectangle.GetName: string;
begin
  result := 'Rectangle'
end;

end.
