program prjOOP;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  uEncapsulation in 'uEncapsulation.pas',
  uInheritance in 'uInheritance.pas',
  uPolymorphism in 'uPolymorphism.pas',
  uInterface in 'uInterface.pas',
  uExercise1 in 'uExercise1.pas',
  uExercise3 in 'uExercise3.pas';

var
  ExerciseOption: String;
  EmployeeList: TEmployeeList;
  Employee: TEmployee;
  EmployeeStats: TEmployeeStatistics;

  //exercise 3
  GeoFormsList: TInterfaceList;
  GeoForm: IGeometricForm;
  GeoFormInt: IInterface;
begin
  try
    Writeln('Which exercise would you like to execute? Options: 1, 2 and 3');
    Readln(ExerciseOption);

    case StrToIntDef(ExerciseOption, 0) of
      1:
      begin
        EmployeeList := TEmployeeList.Create;
        try
          Employee := TEmployee.Create;
          Employee.FirstName := 'First 1';
          Employee.LastName := 'Last 1';
          Employee.Age := 10;
          EmployeeList.Add(Employee);

          Employee := TEmployee.Create;
          Employee.FirstName := 'First 2';
          Employee.LastName := 'Last 2';
          Employee.Age := 30;
          EmployeeList.Add(Employee);

          Employee := TEmployee.Create;
          Employee.FirstName := 'First 3';
          Employee.LastName := 'Last 3';
          Employee.Age := 20;
          EmployeeList.Add(Employee);

          EmployeeStats := TEmployeeStatistics.Create(EmployeeList.GetEnumerator);
          try
            EmployeeStats.Execute;

            WriteLn('Statistics: Total employees: ' + IntToStr(EmployeeStats.Count) + ', highest age: ' + IntToStr(EmployeeStats.HighestAge) +
              ', lowest age: ' + IntToStr(EmployeeStats.LowestAge) + ', average age: ' + FloatToStr(EmployeeStats.AverageAge));
          finally
            FreeAndNil(EmployeeStats);
          end;
        finally
          FreeAndNil(EmployeeList);
        end;

      end;
      2:
      begin
        EmployeeList := TEmployeeList.Create;
        try
          Employee := TEmployee.Create;
          Employee.FirstName := 'First 1';
          Employee.LastName := 'Last 1';
          Employee.Age := 10;
          EmployeeList.Add(Employee);

          Employee := TEmployee.Create;
          Employee.FirstName := 'First 2';
          Employee.LastName := 'Last 2';
          Employee.Age := 30;
          EmployeeList.Add(Employee);

          Employee := TEmployee.Create;
          Employee.FirstName := 'First 3';
          Employee.LastName := 'Last 3';
          Employee.Age := 20;
          EmployeeList.Add(Employee);

          EmployeeStats := TEmployeeStatisticsErrorMargin.Create(EmployeeList.GetEnumerator);
          try
            EmployeeStats.Execute;

            WriteLn('Statistics: Total employees: ' + IntToStr(EmployeeStats.Count) + ', highest age: ' + IntToStr(EmployeeStats.HighestAge) +
              ', lowest age: ' + IntToStr(EmployeeStats.LowestAge) + ', average age: ' + FloatToStr(EmployeeStats.AverageAge));
          finally
            FreeAndNil(EmployeeStats);
          end;
        finally
          FreeAndNil(EmployeeList);
        end;
      end;
      3:
      begin
        GeoFormsList := TInterfaceList.Create;
        try
          GeoForm := TRectangle.Create(10, 15);
          GeoFormsList.Add(GeoForm);

          GeoForm := TSquare.Create(10);
          GeoFormsList.Add(GeoForm);

          for GeoFormInt in GeoFormsList do
          begin
            GeoForm := IGeometricForm(GeoFormInt);

            writeln('The geometric form ' + GeoForm.GetName + ' has the area of ' + FloatToStr(GeoForm.GetArea) + ' m2')
          end;


        finally
          FreeAndNil(GeoFormsList);
        end;

      end;
    else
      writeln('not a valid exercise option')
    end;


    Readln;
  except
    on E:Exception do
      Writeln(E.Classname, ': ', E.Message);
  end;
end.
