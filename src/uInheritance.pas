unit uInheritance;

interface

uses
  classes, uEncapsulation;

type
  {
    TEmployee now became a GENERALIZATION of TEngineeringEmployee

    TEngineeringEmployee has the same functionality as the TEmployee but with extra data: DevelopmentSkills
  }
  TEngineeringEmployee = class(TEmployee)
  private
    FDevelopmentSkills: TStringList;

    function GetHasDevelopmentSkills: boolean;

  protected
    function GetDevelopmentSkills: String; virtual;

  public
    constructor Create;
    destructor Destroy; override;

    function AddDevelopmentSkill(const sSkill: String): boolean;

    property HasDevelopmentSkills: boolean read GetHasDevelopmentSkills;
    property DevelopmentSkills: String read GetDevelopmentSkills;
  end;

implementation

uses
  SysUtils;

{ TEngineeringEmployee }

function TEngineeringEmployee.AddDevelopmentSkill(const sSkill: String): boolean;
begin
  result := FDevelopmentSkills.Add(sSkill) > -1;
end;

constructor TEngineeringEmployee.Create;
begin
  FDevelopmentSkills := TStringList.Create;
end;

destructor TEngineeringEmployee.Destroy;
begin
  FreeAndNil(FDevelopmentSkills);

  inherited;
end;

function TEngineeringEmployee.GetDevelopmentSkills: String;
begin
  result := FDevelopmentSkills.Text;
end;

function TEngineeringEmployee.GetHasDevelopmentSkills: boolean;
begin
  result := FDevelopmentSkills.Count > 0;
end;

end.
