unit uPolymorphism;

interface

uses
  uInheritance;

type
  TEngMagicianEmployee = class(TEngineeringEmployee)
  protected
    function GetDevelopmentSkills: string; override;

  end;

implementation

{ TEngMagicianEmployee }

function TEngMagicianEmployee.GetDevelopmentSkills: string;
var
  EngineeringEmployeeSkills: String;
begin
  //let's take the regular engineering skills
  EngineeringEmployeeSkills := inherited GetDevelopmentSkills;

  //and then add to that the magician skills
  EngineeringEmployeeSkills := EngineeringEmployeeSkills + sLineBreak +
    ' ...and things that are not comprehensible by mere humans!!!!';

  result := EngineeringEmployeeSkills;
end;

end.
