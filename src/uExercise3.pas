unit uExercise3;

interface

uses
  uInterface, Generics.Collections;

type
  TSquare = class(TInterfacedObject, IGeometricForm)
  private
  FArea: Double;

  public
    constructor Create(const dArea: double);
    function GetArea: double;
    function GetName: string;

    property Name: string read GetName;
  end;

implementation

{ TSquare }

constructor TSquare.Create(const dArea: double);
begin
  FArea := dArea;
end;

function TSquare.GetArea: double;
begin
  result := FArea * FArea;
end;

function TSquare.GetName: string;
begin
  result := 'Square'
end;

end.
