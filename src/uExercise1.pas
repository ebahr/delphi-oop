unit uExercise1;

interface

uses
  uEncapsulation, Generics.Collections;

type
  TEmployeeObjList = TObjectList<TEmployee>;
  TEmployeeEnum = TEnumerator<TEmployee>;

  TEmployeeList = class
  private
    FEmployeeObjList: TEmployeeObjList;

  public
    function Add(const Employee: TEmployee): boolean;
    function GetEnumerator: TEmployeeEnum;

    constructor Create;
    destructor Destroy; override;
  end;

  TEmployeeStatistics = class
  private
    FEmployeeEnum: TEmployeeEnum;
    FHighestAge,
    FLowestAge,
    FCount: Integer;
    FAverageAge: Double;

    procedure initialize;

  protected
    function GetAverageAge: Double; virtual;

  public
    constructor Create(const EmployeeEnum: TEmployeeEnum);

    procedure Execute;

    property HighestAge: Integer read FHighestAge;
    property LowestAge: Integer read FLowestAge;
    property AverageAge: Double read GetAverageAge;
    property Count: Integer read FCount;
  end;

  TEmployeeStatisticsErrorMargin = class (TEmployeeStatistics)
  protected
    function GetAverageAge: Double; override;

  end;


implementation

uses
  SysUtils;

{ TEmployeeList }

function TEmployeeList.Add(const Employee: TEmployee): boolean;
begin
  result := FEmployeeObjList.Add(Employee) > -1;
end;

constructor TEmployeeList.Create;
begin
  FEmployeeObjList := TEmployeeObjList.Create(True);
end;

destructor TEmployeeList.Destroy;
begin
  FreeAndNil(FEmployeeObjList);

  inherited;
end;

function TEmployeeList.GetEnumerator: TEmployeeEnum;
begin
  result := FEmployeeObjList.GetEnumerator;
end;

{ TEmployeeStatistics }

constructor TEmployeeStatistics.Create(const EmployeeEnum: TEmployeeEnum);
begin
  FEmployeeEnum := EmployeeEnum;
end;

procedure TEmployeeStatistics.Execute;
var
  Employee: TEmployee;
  AgeSum: Integer;
begin
  AgeSum := 0;

  initialize;
  while FEmployeeEnum.MoveNext do
  begin
    Employee := FEmployeeEnum.Current;
    if FHighestAge = 0 then
      FHighestAge := Employee.Age
    else
      if Employee.Age > FHighestAge then
        FHighestAge := Employee.Age;

    if FLowestAge = 0 then
      FLowestAge := Employee.Age
    else
      if Employee.Age < FLowestAge then
        FLowestAge := Employee.Age;

    AgeSum := AgeSum + Employee.Age;
    Inc(FCount);
  end;

  if FCount > 0 then
    FAverageAge := AgeSum / FCount;
end;

function TEmployeeStatistics.GetAverageAge: Double;
begin
  result := FAverageAge;
end;

procedure TEmployeeStatistics.initialize;
begin
  FHighestAge := 0;
  FLowestAge := 0;
  FCount := 0;
  FAverageAge := 0;
end;

{ TEmployeeStatisticsErrorMargin }

function TEmployeeStatisticsErrorMargin.GetAverageAge: Double;
begin
  result := FAverageAge * 1.1; //sum 10%
end;

end.
